<title>@yield('title')</title>
<meta charset="UTF-8">
<meta content='width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no' name='viewport'>
<meta name="csrf-token" content="{{ csrf_token() }}">
<link rel="stylesheet" href="{{ URL::asset('bootstrap/css/bootstrap.min.css') }}" />
<link rel="stylesheet" href="{{ URL::asset('font-awesome/css/font-awesome.min.css') }}" />
<link rel="stylesheet" href="{{ URL::asset('plugins/datatables/dataTables.bootstrap.css') }}" />
<link rel="stylesheet" href="{{ URL::asset('dist/css/Admin.min.css') }}" />
<link rel="stylesheet" href="{{ URL::asset('dist/css/skins/_all-skins.min.css') }}" />
<link rel="stylesheet" href="{{ URL::asset('plugins/iCheck/square/blue.css') }}" />
<link rel="stylesheet" href="{{ URL::asset('plugins/datepicker/datepicker3.css') }}" />
<link rel="stylesheet" href="{{ URL::asset('css/admin/shopist.css') }}" />
<link rel="stylesheet" href="{{ URL::asset('dropzone/css/dropzone.css') }}" />
<link rel="stylesheet" href="{{ URL::asset('sweetalert/sweetalert.css') }}" />
<link rel="stylesheet" href="{{ URL::asset('plugins/select2/select2.min.css') }}" />
<link rel="stylesheet" href="{{ URL::asset('designer/designer.css') }}" />
<link rel="stylesheet" href="{{ URL::asset('designer/scroll/jquery.mCustomScrollbar.css') }}" />
<link rel="stylesheet" href="{{ URL::asset('plugins/ionslider/ion.rangeSlider.css') }}" />
<link rel="stylesheet" href="{{ URL::asset('plugins/ionslider/ion.rangeSlider.skinModern.css') }}" />
<link rel="stylesheet" href="{{ URL::asset('plugins/bootstrap-slider/slider.css') }}" />
<link rel="stylesheet" href="{{ URL::asset('plugins/morris/morris.css') }}" />
<link rel="stylesheet" href="{{ URL::asset('editor/summernote.css') }}" />
<link rel="stylesheet" href="{{ URL::asset('input-tag/tagmanager.min.css') }}" />

<script type="text/javascript" src="{{ URL::asset('jquery/jquery-1.10.2.js') }}"></script>
<script type="text/javascript" src="{{ URL::asset('jquery/jquery-ui-1.11.4.js') }}"></script>
<script type="text/javascript" src="{{ URL::asset('plugins/datatables/jquery.dataTables.min.js') }}"></script>
<script type="text/javascript" src="{{ URL::asset('plugins/datatables/dataTables.bootstrap.min.js') }}"></script>
<script type="text/javascript" src="https://cdn.datatables.net/buttons/1.5.1/js/dataTables.buttons.min.js"></script>
<script type="text/javascript" src="https://cdn.datatables.net/buttons/1.5.1/js/buttons.flash.min.js"></script>
<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/jszip/3.1.3/jszip.min.js"></script>
<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/pdfmake/0.1.32/pdfmake.min.js"></script>
<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/pdfmake/0.1.32/vfs_fonts.js"></script>
<script type="text/javascript" src="https://cdn.datatables.net/buttons/1.5.1/js/buttons.html5.min.js"></script>
<script type="text/javascript" src="https://cdn.datatables.net/buttons/1.5.1/js/buttons.print.min.js"></script>
<script type="text/javascript" src="{{ URL::asset('dropzone/dropzone.js') }}"></script>
<script type="text/javascript" src="{{ URL::asset('sweetalert/sweetalert.min.js') }}"></script>
<script type="text/javascript" src="{{ URL::asset('bootstrap/js/bootstrap.min.js') }}"></script>
<script type="text/javascript" src="{{ URL::asset('plugins/datepicker/bootstrap-datepicker.js') }}"></script>
<script type="text/javascript" src="{{ URL::asset('plugins/iCheck/icheck.min.js') }}"></script>
<script type="text/javascript" src="{{ URL::asset('plugins/select2/select2.full.min.js') }}"></script>
<script type="text/javascript" src="{{ URL::asset('designer/fabric-1.5.0.min.js') }}"></script>
<script type="text/javascript" src="{{ URL::asset('designer/fabric.curvedText.js') }}"></script>
<script type="text/javascript" src="{{ URL::asset('designer/customiseControls.min.js') }}"></script>
<script type="text/javascript" src="{{ URL::asset('designer/colorpicker/jscolor.js') }}"></script>
<script type="text/javascript" src="{{ URL::asset('designer/scroll/jquery.mCustomScrollbar.concat.min.js') }}"></script>
<script type="text/javascript" src="{{ URL::asset('plugins/ionslider/ion.rangeSlider.min.js') }}"></script>
<script type="text/javascript" src="{{ URL::asset('plugins/bootstrap-slider/bootstrap-slider.js') }}"></script>
<script type="text/javascript" src="{{ URL::asset('plugins/morris/raphael-min.js') }}"></script>
<script type="text/javascript" src="{{ URL::asset('plugins/morris/morris.min.js') }}"></script>
<script type="text/javascript" src="{{ URL::asset('js/admin/shopist.js') }}"></script>
<script type="text/javascript" src="{{ URL::asset('designer/designer.js') }}"></script>
<script type="text/javascript" src="{{ URL::asset('designer/designerControl.js') }}"></script>
<script type="text/javascript" src="{{ URL::asset('dist/js/app.min.js') }}"></script>
<script type="text/javascript" src="{{ URL::asset('editor/summernote.min.js') }}"></script>
<script type="text/javascript" src="{{ URL::asset('input-tag/tagmanager.min.js') }}"></script>
<script type="text/javascript" src="{{ URL::asset('input-tag/bootstrap3-typeahead.min.js') }}"></script>