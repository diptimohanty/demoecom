<style>
  footer{
    padding-top: 0 !important;
  }
</style>
<div class="container">
  <!-- <div class="row">
    <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
      <div class="single-box">
        <div class="all-service-main">
          <div class="all-service service-style">
            <ul class="service-list">
              <li><span class="service-name service-name-earth"></span><p>{!! trans('frontend.worldwide_service_label') !!}</p></li>
              <li><span class="service-name service-name-users"></span><p>{!! trans('frontend.24_7_help_center_label') !!}</p></li>
              <li><span class="service-name service-name-checkmark-circle"></span><p>{!! trans('frontend.safe_payment_label') !!}</p></li>
              <li><span class="service-name service-name-bicycle"></span><p>{!! trans('frontend.quick_delivery_label') !!}</p></li>
            </ul>
          </div>
        </div>
      </div>
      <div class="clear_both"></div>  
    </div>
  </div> -->  

  <div class="row features-block">
    <div class="design-tool-workflow">
        <h2 class="">{!! trans('frontend.footer_about_us') !!}</h2>
      <div class="col-md-12">
        <p class="al-justify">Lorem ipsum dolor sit amet, consectetur adipiscing elit. Cras sapien enim, fermentum a libero id, porta feugiat nisi. Vestibulum euismod, diam sed tincidunt ultrices, dui tellus pharetra sem, sed iaculis elit tortor in diam. Maecenas mattis rhoncus pulvinar. Sed vehicula massa ut porttitor bibendum. Suspendisse non feugiat velit. Nulla magna purus, aliquam ut orci eget, euismod vehicula nunc. Mauris dignissim ornare sagittis. Suspendisse potenti. Proin magna enim, euismod id velit quis, accumsan pulvinar ipsum. Integer blandit, odio et egestas sodales, quam elit congue arcu, a pellentesque erat urna at tortor. Etiam eget odio sapien.</p>
      </div>  
    </div>
  </div>  
  
  <div class="row features-block">
    <div class="design-tool-workflow">
        <h2 class="">{!! trans('frontend.belief_label') !!}</h2>
      <div class="work-img col-md-12">
        <div class="fone col-md-4">
          <div class="featureimg">
            
          </div>
          <a class="green feature" href="javascript:void(0)">
            <h3>{!! trans('frontend.features_title_one') !!}</h3>
            <p>{!! trans('frontend.features_details_one') !!}</p>
          </a>
        </div>
        <div class="fsec col-md-4">
          <div class="featureimg"></div>
          <a class="center grey feature" href="javascript:void(0)">
            <h3>{!! trans('frontend.features_title_sec') !!}</h3>
            <p>{!! trans('frontend.features_details_sec') !!}</p>
          </a>
        </div>
        <div class="fthr col-md-4">
          <div class="featureimg"></div>
          <a class="last orange feature" href="javascript:void(0)">
            <h3>{!! trans('frontend.features_title_thr') !!}</h3>
            <p>{!! trans('frontend.features_details_thr') !!}</p>
          </a>
        </div>
      </div>  
    </div>
  </div>
    
  @if(!empty($appearance_settings_data['home_details']['collection_cat_list']) && count($appearance_settings_data['home_details']['collection_cat_list']) > 0)
  <div class="row">
          <h2 class="">{!! trans('frontend.home_category_section_title') !!}</h2>
    <div id="categories_collection" class="categories-collection">
      @foreach($appearance_settings_data['home_details']['collection_cat_list'] as $collection_cat_details)
        @if($collection_cat_details['status'] == 1)
        <div class="col-xs-12 col-sm-4 col-md-3 col-lg-3 category_items">
          <div class="category">
            <!-- <a href="{{ route('categories-page', $collection_cat_details['slug']) }}"> -->
			<div class="pro-image">
              @if(!empty($collection_cat_details['category_img_url']))  
              <img class="img-responsive" src="{{ get_image_url($collection_cat_details['category_img_url']) }}">
              @else
              <img class="img-responsive" src="{{ default_placeholder_img_src() }}">
              @endif
			  </div>
			  <div class="pro-name">
              <!--<div class="category-collection-mask"></div>-->
              <h3 class="category-title">{!! $collection_cat_details['name'] !!} </h3>
              <a href="{{ route('categories-page', $collection_cat_details['slug']) }}">{!! trans('frontend.view_all_label') !!} <i class="fa fa-long-arrow-right" aria-hidden="true"></i></a>
          </div>
		  <!--  </a> -->
          </div>
        </div>
        @endif
      @endforeach
    </div>
    <div class="clear_both"></div>
  </div>
  @endif
    
  @if(!empty($appearance_settings_data['home_details']['cat_name_and_products']) && count($appearance_settings_data['home_details']['cat_name_and_products']) > 0)  
    <div class="row">
      <div class="top-cat-content">
      @foreach($appearance_settings_data['home_details']['cat_name_and_products'] as $cat_details)
        <div class="col-lg-8 col-md-8 col-sm-8 col-xs-12">
          <div class="single-mini-box2">
            <div class="top-cat-list-sub clearfix">
              <div class="img-div">
                @if(!empty($cat_details['cat_deails']['category_img_url']))  
                <img class="img-responsive" src="{{ get_image_url($cat_details['cat_deails']['category_img_url']) }}">
                @else
                <img class="img-responsive" src="{{ default_placeholder_img_src() }}">
                @endif
              </div>  
              <div class="img-title">
                <h4>{!! trans('frontend.super_deal_label') !!}</h4>  
                <h2>{!! $cat_details['cat_deails']['name'] !!}</h2>
                <span>{!! trans('frontend.exclusive_collection_label') !!}</span>
                <div class="cat-shop-now">
                  <a href="{{ route('categories-page', $cat_details['cat_deails']['slug']) }}">{!! trans('frontend.shop_now_label') !!}</a>
                </div>
              </div>
            </div>
          </div>
        </div>
        @if($cat_details['cat_products']->count() > 0)
          @foreach($cat_details['cat_products'] as $items)
            <div class="col-lg-4 col-md-4 col-sm-4 col-xs-12">
              <div class="single-mini-box2">
                <div class="hover-product">
                  <div class="hover">
                    @if(!empty(get_product_image($items->id))) 
                      <img class="img-responsive" src="{{ get_image_url(get_product_image($items->id)) }}" alt="{{ basename(get_product_image($items->id)) }}" />
                    @else
                      <img class="img-responsive" src="{{ default_placeholder_img_src() }}" alt="" />
                    @endif
                    <div class="overlay">
                      <div class="overlay-content">
                        <button class="info quick-view-popup" data-id="{{ $items->id }}">{{ trans('frontend.quick_view_label') }}</button>  
                        <h2>{!! $items->post_title !!}</h2> 
                        @if(get_product_type($items->id) == 'simple_product')
                          <h3>{!! price_html( get_product_price($items->id), get_frontend_selected_currency() ) !!}</h3>
                        @elseif(get_product_type($items->id) == 'configurable_product')
                          <h3>{!! get_product_variations_min_to_max_price_html(get_frontend_selected_currency(), $items->id) !!}</h3>
                        @elseif(get_product_type($items->id) == 'customizable_product' || get_product_type($items->id) == 'downloadable_product')
                          @if(count(get_product_variations($items->id))>0)
                            <h3>{!! get_product_variations_min_to_max_price_html(get_frontend_selected_currency(), $items->id) !!}</h3>
                          @else
                            <h3>{!! price_html( get_product_price($items->id), get_frontend_selected_currency() ) !!}</h3>
                          @endif
                        @endif
                        <ul>
                          @if(get_product_type( $items->id ) == 'simple_product')  
                            <li><a href="" data-id="{{ $items->id }}" data-tax-id="{!! get_product_tax_percentage($items->id) !!}" class="add-to-cart-bg" data-toggle="tooltip" data-placement="top" title="{{ trans('frontend.add_to_cart_label') }}"><i class="fa fa-shopping-cart shubham"></i>{{ trans('frontend.add_to_cart_label') }}</a></li>
                            <li><a href="" class="product-wishlist" data-id="{{ $items->id }}" data-toggle="tooltip" data-placement="top" title="{{ trans('frontend.add_to_wishlist_label') }}"><i class="fa fa-heart"></i></a></li>
                            <li><a href="" class="product-compare" data-id="{{ $items->id }}" data-toggle="tooltip" data-placement="top" title="{{ trans('frontend.add_to_compare_list_label') }}"><i class="fa fa-exchange" ></i></a></li>
                            <li><a href="{{ route('details-page', $items->post_slug ) }}" class="product-details-view" data-toggle="tooltip" data-placement="top" title="{{ trans('frontend.product_details_label') }}"><i class="fa fa-eye"></i></a></li>
                            @elseif(get_product_type( $items->id ) == 'configurable_product')
                              <li><a href="{{ route( 'details-page', $items->post_slug ) }}" class="select-options-bg" data-toggle="tooltip" data-placement="top" title="{{ trans('frontend.select_options') }}"><i class="fa fa-hand-o-up"></i></a></li>
                              <li><a href="" class="product-wishlist" data-id="{{ $items->id }}" data-toggle="tooltip" data-placement="top" title="{{ trans('frontend.add_to_wishlist_label') }}"><i class="fa fa-heart"></i></a></li>
                              <li><a href="" class="product-compare" data-id="{{ $items->id }}" data-toggle="tooltip" data-placement="top" title="{{ trans('frontend.add_to_compare_list_label') }}"><i class="fa fa-exchange" ></i></a></li>
                              <li><a href="{{ route('details-page', $items->post_slug ) }}" class="product-details-view" data-toggle="tooltip" data-placement="top" title="{{ trans('frontend.product_details_label') }}"><i class="fa fa-eye"></i></a></li>
                            @elseif(get_product_type( $items->id ) == 'customizable_product')  
                              @if(is_design_enable_for_this_product( $items->id ))
                                <li><a href="{{ route('customize-page', $items->post_slug) }}" class="product-customize-bg" data-toggle="tooltip" data-placement="top" title="{{ trans('frontend.customize') }}"><i class="fa fa-gears"></i></a></li>
                                <li><a href="" class="product-wishlist" data-id="{{ $items->id }}" data-toggle="tooltip" data-placement="top" title="{{ trans('frontend.add_to_wishlist_label') }}"><i class="fa fa-heart"></i></a></li>
                                <li><a href="" class="product-compare" data-id="{{ $items->id }}" data-toggle="tooltip" data-placement="top" title="{{ trans('frontend.add_to_compare_list_label') }}"><i class="fa fa-exchange" ></i></a></li>
                                <li><a href="{{ route('details-page', $items->post_slug ) }}" class="product-details-view" data-toggle="tooltip" data-placement="top" title="{{ trans('frontend.product_details_label') }}"><i class="fa fa-eye"></i></a></li>
                              @else
                                  <li><a href="" data-id="{{ $items->id }}" class="add-to-cart-bg" data-toggle="tooltip" data-placement="top" title="{{ trans('frontend.add_to_cart_label') }}"><i class="fa fa-shopping-cart"></i>{{ trans('frontend.add_to_cart_label') }}</a></li>
                                  <li><a href="" class="product-wishlist" data-id="{{ $items->id }}" data-toggle="tooltip" data-placement="top" title="{{ trans('frontend.add_to_wishlist_label') }}"><i class="fa fa-heart"></i></a></li>
                                  <li><a href="" class="product-compare" data-id="{{ $items->id }}" data-toggle="tooltip" data-placement="top" title="{{ trans('frontend.add_to_compare_list_label') }}"><i class="fa fa-exchange" ></i></a></li>
                                  <li><a href="{{ route('details-page', $items->post_slug ) }}" class="product-details-view" data-toggle="tooltip" data-placement="top" title="{{ trans('frontend.product_details_label') }}"><i class="fa fa-eye"></i></a></li>
                              @endif
                            @elseif(get_product_type( $items->id ) == 'downloadable_product') 
                            
                              @if(count(get_product_variations($items->id))>0)
                                <li><a href="{{ route( 'details-page', $items->post_slug ) }}" class="select-options-bg" data-toggle="tooltip" data-placement="top" title="{{ trans('frontend.select_options') }}"><i class="fa fa-hand-o-up"></i></a></li>
                              <li><a href="" class="product-wishlist" data-id="{{ $items->id }}" data-toggle="tooltip" data-placement="top" title="{{ trans('frontend.add_to_wishlist_label') }}"><i class="fa fa-heart"></i></a></li>
                              <li><a href="" class="product-compare" data-id="{{ $items->id }}" data-toggle="tooltip" data-placement="top" title="{{ trans('frontend.add_to_compare_list_label') }}"><i class="fa fa-exchange" ></i></a></li>
                              <li><a href="{{ route('details-page', $items->post_slug ) }}" class="product-details-view" data-toggle="tooltip" data-placement="top" title="{{ trans('frontend.product_details_label') }}"><i class="fa fa-eye"></i></a></li>
                              @else
                                <li><a href="" data-id="{{ $items->id }}" class="add-to-cart-bg" data-toggle="tooltip" data-placement="top" title="{{ trans('frontend.add_to_cart_label') }}"><i class="fa fa-shopping-cart"></i></a>{{ trans('frontend.add_to_cart_label') }}</li>
                            <li><a href="" class="product-wishlist" data-id="{{ $items->id }}" data-toggle="tooltip" data-placement="top" title="{{ trans('frontend.add_to_wishlist_label') }}"><i class="fa fa-heart"></i></a></li>
                            <li><a href="" class="product-compare" data-id="{{ $items->id }}" data-toggle="tooltip" data-placement="top" title="{{ trans('frontend.add_to_compare_list_label') }}"><i class="fa fa-exchange" ></i></a></li>
                            <li><a href="{{ route('details-page', $items->post_slug ) }}" class="product-details-view" data-toggle="tooltip" data-placement="top" title="{{ trans('frontend.product_details_label') }}"><i class="fa fa-eye"></i></a></li>
                              @endif  
                            @endif
                        </ul>
                      </div>
                    </div>
                  </div>
                </div>
              </div>    
            </div>
          @endforeach
        @endif
        <div class="clear_both"></div> <br><br>
      @endforeach
      </div>
    </div>    
  @endif
  
     
  <div class="clear_both"></div>



  <div class="row">
    <h2>{!! trans('frontend.latest_label') !!}</h2> 
    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 latest_product_container">
    @foreach($advancedData['latest_items'] as $key => $latest_product)
    <div class="col-md-3 latest_product_inner">
      <div class="product-container">
        @if(get_product_image($latest_product['id']))
          <img class="img-responsive" src="{{ get_image_url(get_product_image($latest_product['id'])) }}" alt="{{ basename(get_product_image($latest_product['id'])) }}" />
          @else
          <img class="img-responsive" src="{{ default_placeholder_img_src() }}" alt="" />
        @endif
        <div class="single-product-bottom-section">
                        <h3 class="product_title_name"><a href="{{ route('details-page', $latest_product['post_slug']) }}" data-toggle="tooltip" data-placement="top" title="{{ trans('frontend.product_details_label') }}">{!! str_limit(get_product_title($latest_product['id']), $limit = 30, $end = '...') !!}</a></h3>

                        @if(get_product_type($latest_product['id']) == 'simple_product')
                          <p class="no-upper">{!! price_html( get_product_price($latest_product['id']), get_frontend_selected_currency() ) !!}</p>
                        @elseif(get_product_type($latest_product['id']) == 'configurable_product')
                          <p class="no-upper">{!! get_product_variations_min_to_max_price_html(get_frontend_selected_currency(), $latest_product['id']) !!}</p>
                        @elseif(get_product_type($latest_product['id']) == 'customizable_product' || get_product_type($latest_product['id']) == 'downloadable_product')
                          @if(count(get_product_variations($latest_product['id']))>0)
                            <p class="no-upper">{!! get_product_variations_min_to_max_price_html(get_frontend_selected_currency(), $latest_product['id']) !!}</p>
                          @else
                            <p class="no-upper">{!! price_html( get_product_price($latest_product['id']), get_frontend_selected_currency() ) !!}</p>
                          @endif
                        @endif

                        <div class="title-divider"></div>
                        
                        <div class="single-product-add-to-cart">
                          @if(get_product_type($latest_product['id']) == 'simple_product')
                            <a href="javascript:void(0)" data-id="{{ $latest_product['id'] }}" data-tax-id="{!! get_product_tax_percentage($latest_product['id']) !!}" class="btn btn-sm btn-style add-to-cart-bg" data-toggle="tooltip" data-placement="top" title="{{ trans('frontend.add_to_cart_label') }}"><i class="fa fa-shopping-cart"></i>&nbsp;{{ trans('frontend.add_to_cart_label') }}</a>
                            <a href="javascript:void(0)" class="btn btn-sm btn-style product-wishlist" data-id="{{ $latest_product['id'] }}" data-toggle="tooltip" data-placement="top" title="{{ trans('frontend.add_to_wishlist_label') }}"><i class="fa fa-heart"></i></a>
                            <a href="javascript:void(0)" class="info quick-view-popup btn btn-sm btn-style product-details-view" data-id="{{ $latest_product['id'] }}" data-toggle="tooltip" data-placement="top" title="{{ trans('frontend.quick_view_label') }}"><i class="fa fa-eye"></i></a>

                          @elseif(get_product_type($latest_product['id']) == 'configurable_product')
                            <a href="{{ route('details-page', $latest_product['post_slug']) }}" class="btn btn-sm btn-style select-options-bg" data-toggle="tooltip" data-placement="top" title="{{ trans('frontend.select_options') }}"><i class="fa fa-hand-o-up"></i></a>
                            
                            <a href="javascript:void(0)" class="btn btn-sm btn-style  product-wishlist" data-id="{{ $latest_product['id'] }}" data-toggle="tooltip" data-placement="top" title="{{ trans('frontend.add_to_wishlist_label') }}"><i class="fa fa-heart"></i></a>
                            <a href="javascript:void(0)" class="info quick-view-popup btn btn-sm btn-style product-details-view" data-id="{{ $latest_product['id'] }}" data-toggle="tooltip" data-placement="top" title="{{ trans('frontend.quick_view_label') }}"><i class="fa fa-eye"></i></a>
                            
                         @elseif(get_product_type($latest_product['id']) == 'customizable_product')
                            @if(is_design_enable_for_this_product($latest_product['id']))
                              <a href="{{ route('customize-page', $latest_product['post_slug']) }}" class="btn btn-sm btn-style product-customize-bg" data-toggle="tooltip" data-placement="top" title="{{ trans('frontend.customize') }}"><i class="fa fa-gears"></i></a>
                              
                              <a href="javascript:void(0)" class="btn btn-sm btn-style  product-wishlist" data-id="{{ $latest_product['id'] }}" data-toggle="tooltip" data-placement="top" title="{{ trans('frontend.add_to_wishlist_label') }}"><i class="fa fa-heart"></i></a>
                              <a href="javascript:void(0)" class="info quick-view-popup btn btn-sm btn-style product-details-view" data-id="{{ $latest_product['id'] }}" data-toggle="tooltip" data-placement="top" title="{{ trans('frontend.quick_view_label') }}"><i class="fa fa-eye"></i></a>

                            @else
                              <a href="javascript:void(0)" data-id="{{ $latest_product['id'] }}" class="btn btn-sm btn-style add-to-cart-bg" data-toggle="tooltip" data-placement="top" title="{{ trans('frontend.add_to_cart_label') }}"><i class="fa fa-shopping-cart"></i>&nbsp;{{ trans('frontend.add_to_cart_label') }}</a>
                              <a href="javascript:void(0)" class="btn btn-sm btn-style product-wishlist" data-id="{{ $latest_product['id'] }}" data-toggle="tooltip" data-placement="top" title="{{ trans('frontend.add_to_wishlist_label') }}"><i class="fa fa-heart"></i></a>
                              
                              <a href="javascript:void(0)" class="info quick-view-popup btn btn-sm btn-style product-details-view" data-id="{{ $latest_product['id'] }}" data-toggle="tooltip" data-placement="top" title="{{ trans('frontend.quick_view_label') }}"><i class="fa fa-eye"></i></a>
                              
                            @endif
                          @elseif(get_product_type( $latest_product['id'] ) == 'downloadable_product') 
                              @if(count(get_product_variations( $latest_product['id'] ))>0)
                              <a href="{{ route( 'details-page', $latest_product['post_slug'] ) }}" class="btn btn-sm btn-style select-options-bg" data-toggle="tooltip" data-placement="top" title="{{ trans('frontend.select_options') }}"><i class="fa fa-hand-o-up"></i></a>
                              <a href="javascript:void(0)" class="btn btn-sm btn-style product-wishlist" data-id="{{ $latest_product['id'] }}" data-toggle="tooltip" data-placement="top" title="{{ trans('frontend.add_to_wishlist_label') }}"><i class="fa fa-heart"></i></a>
                              
                             
                              @else
                              <a href="javascript:void(0)" data-id="{{ $latest_product['id'] }}" class="btn btn-sm btn-style add-to-cart-bg" data-toggle="tooltip" data-placement="top" title="{{ trans('frontend.add_to_cart_label') }}"><i class="fa fa-shopping-cart"></i> &nbsp;{{ trans('frontend.add_to_cart_label') }}</a>
                              <a href="" class="btn btn-sm btn-style product-wishlist" data-id="{{ $latest_product['id'] }}" data-toggle="tooltip" data-placement="top" title="{{ trans('frontend.add_to_wishlist_label') }}"><i class="fa fa-heart"></i></a>
                              
                              
                              @endif    
                          @endif
                        </div>
        </div>
      </div>
    </div>
    @endforeach
    </div>
    
  </div>
  <div class="col-md-12 text-center mb-5p">
    <a href="{{ route('shop-page') }}" class="btn-vibha-inverse">SHOP NOW</a>
  </div>
</div>
     
<div class="vendor-section container-fluid no-padding">
  <div class="col-md-12 no-padding flex">
    <div class="col-md-6 vendor_outer">
      <!-- vendor list starts -->
      <div class="row vendor_inner">
      <div class="recent-blog">
        <div class="content-title vendor_title">
          <h2> <span>{!! trans('frontend.our_vendors_label') !!}</span></h2>
        </div>
        <div class="recent-blog-content">
          @foreach($vendor_data as $vendor)
          <div class="col-xs-12 col-sm-12 col-md-4 col-lg-4  blog-box extra-padding vendor-box">
            <div class="custom-mask">
              
                @if($vendor->user_photo_url)
                <img class="img-responsive gt" src="{{ get_image_url($vendor->user_photo_url) }}"  alt="{{basename( $vendor->user_photo_url )}}">
                @else
                <img class="img-responsive" src="{{ default_placeholder_img_src() }}"  alt="no image">
                @endif
            </div>      
          </div>
          @endforeach
        </div>
        <!-- shubham-->
        <div class="vendor-adj">
            <a class="view-all-btn" style="text-decoration:none;" href="{{ route('store-list-page-content') }}">{!! trans('frontend.view_all_label') !!} </a>
        </div>
      </div>
    </div> 

  <!-- vendor list ends -->
    </div>
    <div class="col-md-6 no-padding">
      <img src="/uploads/banner_footer.jpg" class="img-responsive">
    </div>
  </div>

</div>
