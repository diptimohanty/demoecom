<div class="container-fluid kurvzWrapper">
  <div class="row">
    <div class="col-xs-12 kurvzHeader">
      <div class="row kurvzHeaderIn">
        <a class="toggleNavigationLink">
          <img class="img-responsive braMenu" src="{{asset('assets/images/braMenu.png')}}">
          Browse
        </a>
        <a class="headerLogoLink"><img class="img-responsive headerLogo" src="{{asset('assets/images/kurvzHeaderLogo.png')}}"></a>
      </div>
    </div>
    <div class="kurvzMenuDiv">
      <div class="kurvzMenuDivIn">
        <ul class="kurvzMenu list-unstyled">
          <li class="kurvzMenuLinks">
            <a class="kurvzMenuLink">Home</a>
          </li>
          <li class="kurvzMenuLinks">
            <a class="kurvzMenuLink">About</a>
          </li>
          <li class="kurvzMenuLinks">
            <a class="kurvzMenuLink">Categories</a>
          </li>
          <li class="kurvzMenuLinks">
            <a class="kurvzMenuLink">Contact Us</a>
          </li>
          <li class="kurvzMenuLinks">
            <a class="kurvzMenuLink">Cart</a>
          </li>
        </ul>
        <div class="kurvzMenuCloseDiv">
          <i class="fas fa-times fa-2x kurvzMenuClose"></i>
        </div>
      </div>
    </div>
    <div class="col-xs-12 homePage">
      <div class="row">
        <div class="col-xs-12 homebanner">
          <div class="row">
            <div id="homeBannerCarousel" class="carousel slide homeBannerCarousel" data-ride="carousel">
              <ol class="carousel-indicators">
                <li data-target="#homeBannerCarousel" data-slide-to="0" class="active"></li>
                <li data-target="#homeBannerCarousel" data-slide-to="1"></li>
                <li data-target="#homeBannerCarousel" data-slide-to="2"></li>
              </ol>
              <div class="carousel-inner">
                <div class="item active">
                  <img class="img-responsive homeBannerCarouselBG" src="{{asset('assets/images/homeBanner1.jpg')}}">
                  <div class="homeBannerOverlay"></div>
                  <div class="homeBannerContent">
                    <h1 class="homeBannerTitle greatVibes">Little Love Story</h1>
                    <p class="homeBannerText">Fun and Flirty Peice for a Seamless Look</p>
                    <a class="btn defaultButton smallButton homeBannerLink" href="#">Shop Now</a>
                  </div>
                </div>
                <div class="item">
                  <img class="img-responsive homeBannerCarouselBG" src="{{asset('assets/images/homeBanner2.jpg')}}">
                  <div class="homeBannerOverlay"></div>
                  <div class="homeBannerContent">
                    <h1 class="homeBannerTitle greatVibes">Very Sexy-Messy Sleve</h1>
                    <p class="homeBannerText">Fun and Flirty Peice for a Seamless Look</p>
                    <a class="btn defaultButton smallButton homeBannerLink" href="#">Shop Now</a>
                  </div>
                </div>
                <div class="item">
                  <img class="img-responsive homeBannerCarouselBG" src="{{asset('assets/images/homeBanner3.jpg')}}">
                  <div class="homeBannerOverlay"></div>
                  <div class="homeBannerContent">
                    <h1 class="homeBannerTitle greatVibes">Very Sexy-Lase Teddy</h1>
                    <p class="homeBannerText">Fun and Flirty Peice for a Seamless Look</p>
                    <a class="btn defaultButton smallButton homeBannerLink" href="#">Shop Now</a>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
        <div class="clearfix"></div>
        <div class="col-xs-12 homeGroups">
          <div class="homeGroupOverlay"></div>
          <div class="row homeGroupsIn">
            <div class="container homeGroupsContainer">
              <div class="row homeGroupsContainerIn">
                <h1 class="col-xs-12 textCenter">Pick your choice</h1>

                {{print_r($appearance_settings_data)}}
                <div class="col-xs-4 homeGroupLinkDiv">
                  <div class="row homeGroupLinkDivIn">
                    <a class="homeGroupLink" href="#">
                      <img class="img-responsive marginLRAuto homeGroupImg" src="{{asset('assets/images/homeGroupBra.png')}}">
                      <p class="col-xs-12 homeGroupTitle">Bra</p>
                    </a>
                  </div>
                </div>
                <div class="col-xs-4 homeGroupLinkDiv">
                  <div class="row homeGroupLinkDivIn">
                    <a class="homeGroupLink" href="#">
                      <img class="img-responsive marginLRAuto homeGroupImg" src="{{asset('assets/images/homeGroupPanty.png')}}">
                      <p class="col-xs-12 homeGroupTitle">Panty</p>
                    </a>
                  </div>
                </div>
                <div class="col-xs-4 homeGroupLinkDiv">
                  <div class="row homeGroupLinkDivIn">
                    <a class="homeGroupLink" href="#">
                      <img class="img-responsive marginLRAuto homeGroupImg" src="{{asset('assets/images/homeGroupCamisole.png')}}">
                      <p class="col-xs-12 homeGroupTitle">Camisole</p>
                    </a>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
        <div class="clearfix"></div>
        <div class="col-xs-12 homeFeaturedCategory">
          <div class="row homeFeaturedCategoryIn">
            <div class="col-md-8 col-md-offset-4 col-xs-12 col-xs-offset-0">
              <h1 class="textCenter homeFeaturedCategoryTitle">Kissing in Black</h1>
              <p class="homeFeaturedCategoryText">Get any bra for $50 and any 2 panties for $30 + one lucky winner will get their order for free.</p>
              <a class="btn defaultButton smallButton homeFeaturedCategoryLink">Shop Now</a>
            </div>
          </div>
        </div>
        <div class="clearfix"></div>
        <div class="container homeFeaturedProducts">
          <div class="row">
            <h1 class="col-xs-12 textCenter greatVibes">Featured Products</h1>
            <p class="col-xs-12 textCenter uppercase">a touch of coverage never felt so sexy</p>
          </div>
          <div class="row homeFeaturedProductSelector">
            <a class="homeFeaturedProductSelectorLink active">bra</a>
            <a class="homeFeaturedProductSelectorLink">panty</a>
            <a class="homeFeaturedProductSelectorLink">camisole</a>
          </div>
          <div class="row homeFeaturedProductsIn">
            <div class="col-lg-2 col-md-3 col-sm-4 col-xs-6">
              <div class="col-xs-12 homeFeaturedProductImgDiv">
                <img class="img-responsive homeFeaturedProductImg" src="{{asset('assets/images/homeFeaturedProduct01.JPG')}}">
                <img class="img-responsive homeFeaturedProductImgFront" src="{{asset('assets/images/homeFeaturedProduct01Front.JPG')}}">
                <div class="productOptions">
                  <a class="productOption"><i class="fas fa-shopping-bag"></i></a>
                  <a class="productOption"><i class="fas fa-heart"></i></a>
                  <a class="productOption"><i class="fas fa-search"></i></a>
                </div>
              </div>
              <a href="#" class="col-xs-12 textCenter homefeaturedProductTitle">Roxi Royal Blue</a>
              <a class="col-xs-12 textCenter homefeaturedProductRating"><i class="fas fa-star"></i><i class="fas fa-star"></i><i class="fas fa-star"></i><i class="fas fa-star"></i><i class="far fa-star"></i></a>
              <p class="col-xs-12 textCenter homefeaturedProductPrice">&#8377; 700</p>
            </div>
            <div class="col-lg-2 col-md-3 col-sm-4 col-xs-6">
              <div class="col-xs-12 homeFeaturedProductImgDiv">
                <img class="img-responsive homeFeaturedProductImg" src="{{asset('assets/images/homeFeaturedProduct02.JPG')}}">
                <img class="img-responsive homeFeaturedProductImgFront" src="{{asset('assets/images/homeFeaturedProduct02Front.JPG')}}">
                <div class="productOptions">
                  <a class="productOption"><i class="fas fa-shopping-bag"></i></a>
                  <a class="productOption"><i class="fas fa-heart"></i></a>
                  <a class="productOption"><i class="fas fa-search"></i></a>
                </div>
              </div>
              <a href="#" class="col-xs-12 textCenter homefeaturedProductTitle">Snappy Camisole</a>
              <a class="col-xs-12 textCenter homefeaturedProductRating"><i class="fas fa-star"></i><i class="fas fa-star"></i><i class="fas fa-star"></i><i class="far fa-star"></i><i class="far fa-star"></i></a>
              <p class="col-xs-12 textCenter homefeaturedProductPrice">&#8377; 700</p>
            </div>
            <div class="col-lg-2 col-md-3 col-sm-4 col-xs-6">
              <div class="col-xs-12 homeFeaturedProductImgDiv">
                <img class="img-responsive homeFeaturedProductImg" src="{{asset('assets/images/homeFeaturedProduct03.JPG')}}">
                <img class="img-responsive homeFeaturedProductImgFront" src="{{asset('assets/images/homeFeaturedProduct03Front.JPG')}}">
                <div class="productOptions">
                  <a class="productOption"><i class="fas fa-shopping-bag"></i></a>
                  <a class="productOption"><i class="fas fa-heart"></i></a>
                  <a class="productOption"><i class="fas fa-search"></i></a>
                </div>
              </div>
              <a href="#" class="col-xs-12 textCenter homefeaturedProductTitle">Comfort Print Panties</a>
              <a class="col-xs-12 textCenter homefeaturedProductRating"><i class="fas fa-star"></i><i class="fas fa-star"></i><i class="fas fa-star"></i><i class="fas fa-star"></i><i class="far fa-star"></i></a>
              <p class="col-xs-12 textCenter homefeaturedProductPrice">&#8377; 700</p>
            </div>
            <div class="col-lg-2 col-md-3 col-sm-4 col-xs-6">
              <div class="col-xs-12 homeFeaturedProductImgDiv">
                <img class="img-responsive homeFeaturedProductImg" src="{{asset('assets/images/homeFeaturedProduct04.JPG')}}">
                <img class="img-responsive homeFeaturedProductImgFront" src="{{asset('assets/images/homeFeaturedProduct04Front.JPG')}}">
                <div class="productOptions">
                  <a class="productOption"><i class="fas fa-shopping-bag"></i></a>
                  <a class="productOption"><i class="fas fa-heart"></i></a>
                  <a class="productOption"><i class="fas fa-search"></i></a>
                </div>
              </div>
              <a href="#" class="col-xs-12 textCenter homefeaturedProductTitle">Sports Bra</a>
              <a class="col-xs-12 textCenter homefeaturedProductRating"><i class="fas fa-star"></i><i class="fas fa-star"></i><i class="fas fa-star"></i><i class="fas fa-star"></i><i class="far fa-star"></i></a>
              <p class="col-xs-12 textCenter homefeaturedProductPrice">&#8377; 700</p>
            </div>
            <div class="col-lg-2 col-md-3 col-sm-4 col-xs-6">
              <div class="col-xs-12 homeFeaturedProductImgDiv">
                <img class="img-responsive homeFeaturedProductImg" src="{{asset('assets/images/homeFeaturedProduct05.JPG')}}">
                <img class="img-responsive homeFeaturedProductImgFront" src="{{asset('assets/images/homeFeaturedProduct05Front.JPG')}}">
                <div class="productOptions">
                  <a class="productOption"><i class="fas fa-shopping-bag"></i></a>
                  <a class="productOption"><i class="fas fa-heart"></i></a>
                  <a class="productOption"><i class="fas fa-search"></i></a>
                </div>
              </div>
              <a href="#" class="col-xs-12 textCenter homefeaturedProductTitle">Trendy Camisole</a>
              <a class="col-xs-12 textCenter homefeaturedProductRating"><i class="fas fa-star"></i><i class="fas fa-star"></i><i class="fas fa-star"></i><i class="fas fa-star"></i><i class="far fa-star"></i></a>
              <p class="col-xs-12 textCenter homefeaturedProductPrice">&#8377; 700</p>
            </div>
            <div class="col-lg-2 col-md-3 col-sm-4 col-xs-6">
              <div class="col-xs-12 homeFeaturedProductImgDiv">
                <img class="img-responsive homeFeaturedProductImg" src="{{asset('assets/images/homeFeaturedProduct06.JPG')}}">
                <img class="img-responsive homeFeaturedProductImgFront" src="{{asset('assets/images/homeFeaturedProduct06Front.JPG')}}">
                <div class="productOptions">
                  <a class="productOption"><i class="fas fa-shopping-bag"></i></a>
                  <a class="productOption"><i class="fas fa-heart"></i></a>
                  <a class="productOption"><i class="fas fa-search"></i></a>
                </div>
              </div>
              <a href="#" class="col-xs-12 textCenter homefeaturedProductTitle">Panties Classy -IE</a>
              <a class="col-xs-12 textCenter homefeaturedProductRating"><i class="fas fa-star"></i><i class="fas fa-star"></i><i class="fas fa-star"></i><i class="fas fa-star"></i><i class="far fa-star"></i></a>
              <p class="col-xs-12 textCenter homefeaturedProductPrice">&#8377; 700</p>
            </div>
          </div>
        </div>
        <div class="clearfix"></div>
        <div class="col-xs-12 homeNewsletter">
          <div class="row homeNewsletterIn">
            <div class="col-md-6 col-md-offset-6 col-xs-12 col-xs-offset-0">
              <h1 class="col-xs-12 textCenter greatVibes">Subscribe for Newsletter</h1>
              <p class="col-xs-12 textCenter">Subscribe to our newsletters now and stay up-to-date with new collections, the latest lookbooks and exclusive offers.</p>
              <form class="col-xs-12 homeNewsletterForm">
                <input class="form-control col-md-6 col-xs-8" type="email" required>
                <button type="submit" class="col-xs-4 btn defaultButton smallButton">subscribe</button>
              </form>
            </div>
          </div>
        </div>
        <div class="clearfix"></div>
        <div class="col-xs-12 homeTestimonials">
          <div class="row homeTestimonialsIn">
            <div class="container homeTestimonialsContainer">
              <div class="row homeTestimonialsContainerIn">
                <h1 class="textCenter greatVibes">Testimonials</h1>
                <ul class="wi100 list-unstyled list-inline homeTestimonialsImgCarousel">
                  <li class="homeTestimonialImgDiv">
                    <img class="img-responsive homeTestimonialImg" src="{{asset('assets/images/homeTestimonial01.png')}}">
                  </li>
                  <li class="homeTestimonialImgDiv active">
                    <img class="img-responsive homeTestimonialImg" src="{{asset('assets/images/homeTestimonial02.jpg')}}">
                  </li>
                  <li class="homeTestimonialImgDiv">
                    <img class="img-responsive homeTestimonialImg" src="{{asset('assets/images/homeTestimonial03.png')}}">
                  </li>
                </ul>
                <ul class="wi100 list-unstyled list-inline homeTestimonialsContentCarousel">
                  <li class="homeTestimonialContentDiv">
                    <p class="col-md-6 col-xs-12 textCenter">Est at sanctus delenit, et affert legimus per. Sit fierent voluptua te. Nonumes omnesque scribentur ius at. Ad has labores habemus corpora, duo facete epicurei probatus cu. Ad purto ocurreret expetendis has.</p>
                    <a href="#" class="wi100 textCenter">Read More</a>
                  </li>
                  <li class="homeTestimonialContentDiv active">
                    <p class="col-md-6 col-xs-12 textCenter">Iusto vidisse ex per, cum utamur expetendis et. Quas magna nonumes ea pro. Facer graecis incorrupte eos an. In vel accusam democritum, pro euismod delicatissimi ex.</p>
                    <a href="#" class="wi100 textCenter">Read More</a>
                  </li>
                  <li class="homeTestimonialContentDiv">
                    <p class="col-md-6 col-xs-12 textCenter">Per no adversarium accommodare. Ea cum hendrerit scripserit omittantur. Menandri assueverit interesset ius an. Veri voluptua duo ea, ex homero omnium mei. Possim scaevola an mei. Vel quod choro reformidans cu.</p>
                    <a href="#" class="wi100 textCenter">Read More</a>
                  </li>
                </ul>
                <div class="wi100 carousel-controls-div">
                  <div class="row carousel-controls-div-in">
                    <a class="text-center carousel-control-prev" onclick="homeTestimonialCarousel(0);">
                      <i class="fas fa-arrow-left fa-lg push_right"></i>
                    </a>
                    <p class="carouselActiveCount">1</p>
                    <a class="text-center carousel-control-next" onclick="homeTestimonialCarousel(1);">
                      <i class="fas fa-arrow-right fa-lg push_left"></i>
                    </a>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
    <div class="clearfix"></div>
    <div class="col-xs-12 kurvzFooter">
      <div class="row kurvzFooterIn">
        <div class="container kurvzFooterContainer">
          <div class="row kurvzFooterContainerIn">
            <div class="col-xs-12 footerLogoDiv">
              <div class="row footerLogoDivIn">
                <img class="img-responsive footerLogo" src="{{asset('assets/images/footerLogo.png')}}">
              </div>
            </div>
            <div class="col-xs-12 footerLinksDiv">
              <div class="row footerLinksDivIn">
                <div class="col-md-4 col-xs-6 footerLinks">
                  <img class="img-responsive footerGroupIcons" src="{{asset('assets/images/homeGroupBra.png')}}">
                  <ul class="w-100 list-unstyled footerLinksList">
                    <li class="footerLink">
                      <a href="#">Cut &amp; Sew</a>
                    </li>
                    <li class="footerLink">
                      <a href="#">Economy</a>
                    </li>
                    <li class="footerLink">
                      <a href="#">Moulded</a>
                    </li>
                    <li class="footerLink viewMore">
                      <a href="#">View More</a>
                    </li>
                  </ul>
                </div>
                <div class="col-md-4 col-xs-6 footerLinks">
                  <img class="img-responsive footerGroupIcons" src="{{asset('assets/images/homeGroupPanty.png')}}">
                  <ul class="w-100 list-unstyled footerLinksList">
                    <li class="footerLink">
                      <a href="#">Classy -IE</a>
                    </li>
                    <li class="footerLink">
                      <a href="#">Classy OE</a>
                    </li>
                    <li class="footerLink">
                      <a href="#">Comfort Print</a>
                    </li>
                    <li class="footerLink viewMore">
                      <a href="#">View More</a>
                    </li>
                  </ul>
                </div>
                <div class="col-md-4 col-xs-6 footerLinks">
                  <img class="img-responsive footerGroupIcons" src="{{asset('assets/images/homeGroupCamisole.png')}}">
                  <ul class="w-100 list-unstyled footerLinksList">
                    <li class="footerLink">
                      <a href="#">Dual</a>
                    </li>
                    <li class="footerLink">
                      <a href="#">Princess</a>
                    </li>
                    <li class="footerLink">
                      <a href="#">Racer Back</a>
                    </li>
                    <li class="footerLink viewMore">
                      <a href="#">View More</a>
                    </li>
                  </ul>
                </div>
              </div>
            </div>
            <div class="col-xs-12 footerSocialDetailsDiv">
              <div class="row footerSocialDetailsDivIn">
                <p class="col-xs-12 textCenter footerDetails">Propriae indoctum est cu, graecis legendos eloquentiam est at. Oratio oporteat quo et. Porro maiorum duo ut, et quis ludus affert vel, id vidit singulis ius. Causae honestatis ad vim, oblique conclusionemque eu has, id primis putent legimus pro. Dicant necessitatibus vis no, solum corpora ea duo. Essent qualisque ad eam, te mea commodo civibus lobortis, has at inani forensibus.</p>
                <div class="col-xs-12 footerSocialDiv">
                  <div class="row footerSocialDivIn">
                    <ul class="footerSocial list-inline">
                      <li class="footerSocialIcons">
                        <a class="footerSocialIconsIn" href="http://www.facebook.com"><img class="img-responsive footerSocialIcon" src="{{asset('assets/images/facebook.png')}}"></a>
                      </li>
                      <li class="footerSocialIcons">
                        <a class="footerSocialIconsIn" href="http://www.twitter.com"><img class="img-responsive footerSocialIcon" src="{{asset('assets/images/twitter.png')}}"></a>
                      </li>
                      <li class="footerSocialIcons">
                        <a class="footerSocialIconsIn" href="http://www.instagram.com"><img class="img-responsive footerSocialIcon" src="{{asset('assets/images/instagram.png')}}"></a>
                      </li>
                      <li class="footerSocialIcons">
                        <a class="footerSocialIconsIn" href="http://www.linkedin.com"><img class="img-responsive footerSocialIcon" src="{{asset('assets/images/linkedin.png')}}"></a>
                      </li>
                      <li class="footerSocialIcons">
                        <a class="footerSocialIconsIn" href="http://www.pinterest.com"><img class="img-responsive footerSocialIcon" src="{{asset('assets/images/pinterest.png')}}"></a>
                      </li>
                    </ul>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
      <div class="row kurvzCopyright">
        <p class="col-xs-12 textCenter">Copyright &copy; <a href="http://www.kurvz.in">Aaruthra Apparels Pvt.Ltd</a> 2018 | Powered by <a href="http://www.orbiosolutions.com">Orbio Solutions Pvt.Ltd</a></p>
      </div>
    </div>
  </div>
</div>
