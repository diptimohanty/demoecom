<div class="footer-top full-width shubham">
  <div class="footer-top-background">
    <div class="container">
      <div class="row">
        <div class="col-xs-12 col-sm-12 col-md-3">
          <h3 class="widget-title">
              <img src="{{ get_site_logo_image() }}" title="{{ trans('frontend.your_store_label') }}" alt="{{ trans('frontend.your_store_label') }}">
          </h3>
         
          {!! $appearance_all_data['footer_details']['footer_about_us_description'] !!}
          
         
          
        </div>
        
        <div class="col-xs-12 col-sm-12 col-md-3">
          <h3 class="widget-title">{!! trans('frontend.company_label') !!}</h3>
          <div class="is-divider small"></div>
          <div class="latest-footer-blogs">
            
            <ul>
              <li><a href="{{route('shop-page')}}">{!! trans('frontend.shopist_shop_title') !!}</a></li>
              <li><a href="{{route('about-page-content')}}">{!! trans('frontend.footer_about_us') !!}</a></li>
              <li><a href="{{route('contact-page-content')}}">{!! trans('frontend.contact_us_label') !!}</a></li>
              <li><a href="{{route('store-list-page-content')}}">{!! trans('frontend.vendor_account_store_list_title_label') !!}</a></li>
            </ul>
            
          </div>
        </div>
        
        <div class="col-xs-12 col-sm-12 col-md-3">
          <h3 class="widget-title">{!! trans('frontend.home_category_section_title') !!}</h3>
          <div class="is-divider small"></div>
          <div class="footer-tags-list">
              
              @foreach($appearance_settings_data['home_details']['collection_cat_list'] as $collection_cat_details)
                <a href="{{ route('categories-page', $collection_cat_details['slug']) }}">{!! $collection_cat_details['name'] !!}</a>
              @endforeach
          </div>
        </div>
        <div class="col-xs-12 col-sm-12 col-md-3 contact_section">
          <h3 class="widget-title">{!! trans('frontend.contact_us_label') !!}</h3>
          <div class="is-divider small"></div>
          <p><i class="fa fa-mobile" aria-hidden="true"></i> &nbsp; <a href="tel:{{ $appearance_all_data['footer_details']['footer_contact']}}">{{ $appearance_all_data['footer_details']['footer_contact']}}</a></p>
          <p><i class="fa fa-envelope" aria-hidden="true"></i> &nbsp; <a href="mailto:{{ $appearance_all_data['footer_details']['footer_email']}}">{{ $appearance_all_data['footer_details']['footer_email']}}</a></p>
          <ul class="social-media">
            <li><a class="facebook" href="//{{ $appearance_all_data['footer_details']['follow_us_url']['fb'] }}" data-toggle="tooltip" data-placement="top" title="{{ trans('frontend.fb_tooltip_msg') }}"><i class="fa fa-facebook"></i></a></li>
            <li><a class="twitter" href="//{{ $appearance_all_data['footer_details']['follow_us_url']['twitter'] }}" data-toggle="tooltip" data-placement="top" title="{{ trans('frontend.twitter_tooltip_msg') }}"><i class="fa fa-twitter"></i></a></li>
            <li><a class="linkedin" href="//{{ $appearance_all_data['footer_details']['follow_us_url']['linkedin'] }}" data-toggle="tooltip" data-placement="top" title="{{ trans('frontend.linkedin_tooltip_msg') }}"><i class="fa fa-linkedin"></i></a></li>
            <li><a class="youtube-play" href="//{{ $appearance_all_data['footer_details']['follow_us_url']['youtube'] }}" data-toggle="tooltip" data-placement="top" title="{{ trans('frontend.youtube_play_tooltip_msg') }}"><i class="fa fa-youtube-play"></i></a></li>
            
          </ul>
        </div>
        
        
      </div>
      <div class="copyright-div text-center">
            <span>{!! trans('frontend.footer_msg', ['title' => get_site_title(),'year'=>date('Y')]) !!} <i class="fa fa-bolt" aria-hidden="true"></i> {!! trans('frontend.footer_powered_by') !!}: <a href="http://orbiosolutions.com/" target="_blank" style="color: #888888;">{!!  trans('frontend.footer_powered_name') !!}</a></span>  
        </div>
    </div>
  </div>
</div>
<!-- <div class="footer-copyright full-width">
  <div class="footer-copyright-background">
    <div class="container">
      <div class="row">
        <div class="col-md-12 footer-text">
          <div class="text-center">{!! trans('frontend.footer_msg', ['title' => get_site_title()]) !!}</div>
          <div class="text-center">{!! trans('frontend.footer_powered_by') !!} <strong>{!! get_site_title() !!}</strong></div>
        </div>
      </div>
    </div>
  </div>  
</div> -->