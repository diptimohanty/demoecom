@include('includes.frontend.header-content-custom-css')
<div id="header_content" class="header-before-slider header-background">
  <!-- <div class="top-header">
    <div class="container">
      <div class="row">
        <div class="col-xs-5 col-sm-6 col-md-6 col-lg-6">
          <div class="dropdown change-multi-currency">
            @if(get_frontend_selected_currency())
            <a class="dropdown-toggle" href="#" data-hover="dropdown" data-toggle="dropdown">
              <span class="hidden-xs">{!! get_currency_name_by_code( get_frontend_selected_currency() ) !!}</span>
              <span class="hidden-sm hidden-md hidden-lg fa fa-money fa-lg"></span> 
              @if(count(get_frontend_selected_currency_data()) >0)
              <span class="caret"></span>
              @endif
            </a>
            @endif
            <div class="dropdown-content">
              @if(count(get_frontend_selected_currency_data()) >0)
                @foreach(get_frontend_selected_currency_data() as $val)
                  <a href="#" data-currency_name="{{ $val }}">{!! get_currency_name_by_code( $val ) !!}</a>
                @endforeach
              @endif
            </div>
          </div>
          <div class="dropdown language-list">
            @if(count(get_frontend_selected_languages_data()) > 0)
              @if(get_frontend_selected_languages_data()['lang_code'] == 'en')
                <a class="dropdown-toggle" href="#" data-hover="dropdown" data-toggle="dropdown">
                  <img src="{{ asset('public/images/'. get_frontend_selected_languages_data()['lang_sample_img']) }}" alt="lang"> <span class="hidden-xs"> &nbsp; {!! get_frontend_selected_languages_data()['lang_name'] !!}</span> <span class="caret"></span></a>
              @else
                <a class="dropdown-toggle" href="#" data-hover="dropdown" data-toggle="dropdown">
                  <img src="{{ get_image_url(get_frontend_selected_languages_data()['lang_sample_img']) }}" alt="lang"> <span class="hidden-xs"> &nbsp; {!! get_frontend_selected_languages_data()['lang_name'] !!}</span> <span class="caret"></span></a>
              @endif
            @endif
            
            @if(count(get_available_languages_data_frontend()) > 0)
              <div class="dropdown-content">
                @foreach(get_available_languages_data_frontend() as $key => $val)
                  @if($val['lang_code'] == 'en')
                    <a href="#" data-lang_name="{{ $val['lang_code'] }}"><img src="{{ asset('public/images/'. $val['lang_sample_img']) }}" alt="lang"> &nbsp;{!! ucwords($val['lang_name']) !!}</a>
                  @else
                    <a href="#" data-lang_name="{{ $val['lang_code'] }}"><img src="{{ get_image_url($val['lang_sample_img']) }}" alt="lang"> &nbsp;{!! ucwords($val['lang_name']) !!}</a>
                  @endif
                @endforeach
              </div>
            @endif
          </div>     
        </div>
      
        <div class="col-xs-7 col-sm-6 col-md-6 col-lg-6">
          <div class="clearfix">
            <div class="pull-right">
              <ul class="right-menu top-right-menu">
                <li class="wishlist-content">
                  <a class="main" href="{{ route('my-saved-items-page') }}">
                    <i class="fa fa-heart"></i> 
                    <span class="hidden-xs">{!! trans('frontend.frontend_wishlist') !!}</span> 
                  </a>    
                </li> 
                
                <li class="users-vendors-login"><a href="#" class="main selected dropdown-toggle" data-toggle="dropdown" data-hover="dropdown"><i class="fa fa-user" aria-hidden="true"></i> <span class="hidden-xs">{!! trans('frontend.menu_my_account') !!}</span><span class="caret"></span></a>
                  <ul class="dropdown-menu">
                    @if (Session::has('shopist_frontend_user_id'))
                    <li><a href="{{ route('user-account-page') }}">{!! trans('frontend.user_account_label') !!}</a></li>
                    @else
                    <li><a href="{{ route('user-login-page') }}">{!! trans('frontend.frontend_user_login') !!}</a></li>
                    @endif
                    
                    @if (Session::has('shopist_admin_user_id') && !empty(get_current_vendor_user_info()['user_role_slug']) && get_current_vendor_user_info()['user_role_slug'] == 'vendor')
                     <li><a target="_blank" href="{{ route('admin.dashboard') }}">{!! trans('frontend.vendor_account_label') !!}</a></li>
                    @else
                     <li><a target="_blank" href="{{ route('admin.login') }}">{!! trans('frontend.frontend_vendor_login') !!}</a></li>
                    @endif
                    
                    <li><a href="{{ route('vendor-registration-page') }}">{!! trans('frontend.vendor_registration') !!}</a></li>
                  </ul>
                </li>
                
                <li class="mini-cart-content">
                    @include('pages.ajax-pages.mini-cart-html')
                </li>
              </ul>
            </div>  
          </div>
        </div> 
      </div>         
    </div>      
  </div> --> 
   
  <div class="container">  
    <div class="row">
      <div class="search-content">
        <div class="col-xs-12 col-sm-0 col-md-3 col-lg-3">
          @if(get_site_logo_image())
            <div class="logo hidden-xs hidden-sm"><a href="{{ route('home-page') }}"><img src="{{ get_site_logo_image() }}" title="{{ trans('frontend.your_store_label') }}" alt="{{ trans('frontend.your_store_label') }}" ></a></div>
          @endif
        </div> 

        <!-- <div class="col-xs-8 col-sm-10 col-md-4 col-lg-4 pull-right">
          <form id="search_option" action="{{ route('shop-page') }}" method="get">
            <div class="input-group">
              <input type="text" id="srch_term" name="srch_term" class="form-control" placeholder="{{ trans('frontend.search_for_label') }}">
              <span class="input-group-btn">
                <button id="btn-search" type="submit" class="btn btn-default">
                  SEARCH
                </button>
              </span>
            </div>
          </form>
        </div> search end--> 

      <div class="col-xs-12 col-sm-10 col-md-8 col-lg-8 pull-right">
        <nav class="navbar navbar-default" role="navigation">
          <div class="navbar-header">
            <button type="button" class="btn navbar-toggle collapsed" 
               data-toggle="collapse" data-target="#header-navbar-collapse">
               <span class="sr-only">Toggle navigation</span>
               <span class="icon-bar"></span>
               <span class="icon-bar"></span>
               <span class="icon-bar"></span>
            </button>
            <img class="navbar-brand visible-xs visible-sm" src="{{ get_site_logo_image() }}" alt="{{ trans('frontend.your_store_label') }}">  
          </div>
          <div class="collapse navbar-collapse" id="header-navbar-collapse">
            <ul class="all-menu nav navbar-nav pull-right">
              @if(Request::is('/'))
                <li class="first"><a href="{{ route('home-page') }}" class="main selected menu-name">{!! trans('frontend.home') !!}</a></li>
              @else
                <li class="first"><a href="{{ route('home-page') }}" class="main menu-name">{!! trans('frontend.home') !!}</a></li>
              @endif

              <li class="dropdown mega-dropdown">
                <a href="javascript:void(0)" class="dropdown-toggle menu-name" id="mega_menu_toggle">{!! trans('frontend.shop_by_cat_label') !!}  <span class="caret"></span></a>
                <ul class="dropdown-menu-custom mega-dropdown-menu mega-menu-cat row clearfix">
                  @if(count($productCategoriesTree) > 0)
                    <?php $i = 1; $j = 0;?>
                    @foreach($productCategoriesTree as $cat)
                      @if($i == 1)
                      <?php $j++; ?>
                      <li class="col-xs-12 col-sm-2">  
                      @endif

                      <ul>
                        @if(isset($cat['parent']) && $cat['parent'] == 'Parent Category')  
                        <li class="dropdown-header {{$cat['slug']}}">
                          <a href="{{ route('categories-page', $cat['slug']) }}" class="parent_cat_title">
                            @if( !empty($cat['img_url']) )
                            <img src="{{ get_image_url($cat['img_url']) }}"> 
                            @else
                            <img src="{{ default_placeholder_img_src() }}"> 
                            @endif
                            
                            {!! $cat['name'] !!}
                          </a>
                        </li>
                        @endif
                        @if(isset($cat['children']) && count($cat['children']) > 0)
                          @foreach($cat['children'] as $cat_sub)
                            <li class="product-sub-cat"><a href="{{ route('categories-page', $cat_sub['slug']) }}">{!! $cat_sub['name'] !!}</a></li>
                            @if(isset($cat_sub['children']) && count($cat_sub['children']) > 0)
                              @include('pages.common.category-frontend-loop-home', $cat_sub)
                            @endif
                          @endforeach
                        @endif
                      </ul>

                      @if($i == 1)
                      </li>
                      <?php $i = 0;?>
                      @endif

                      @if($j == 3 || $j == 4)
                      
                      <?php $j = 0; ?>
                      @endif

                      <?php $i ++;?>
                    @endforeach
                  @endif
                </ul>
              </li>
              @if(Request::is('about'))
                <li><a href="{{ route('about-page-content') }}" class="main selected menu-name">{!! trans('frontend.about') !!}</a></li>
              @else
                <li><a href="{{ route('about-page-content') }}" class="main menu-name">{!! trans('frontend.about') !!}</a></li>
              @endif  
              <!-- @if(Request::is('shop'))
                <li><a href="{{ route('shop-page') }}" class="main selected menu-name">{!! trans('frontend.all_products_label') !!}</a></li>
              @else
                <li><a href="{{ route('shop-page') }}" class="main menu-name">{!! trans('frontend.all_products_label') !!}</a></li>
              @endif -->

              <!-- @if(Request::is('checkout'))
                <li><a href="{{ route('checkout-page') }}" class="main selected menu-name">{!! trans('frontend.checkout') !!}</a></li>
              @else
                <li><a href="{{ route('checkout-page') }}" class="main menu-name">{!! trans('frontend.checkout') !!}</a></li>
              @endif -->
              @if (Session::has('shopist_frontend_user_id'))
                @if(Request::is('cart'))
                  <!-- <li><a href="{{ route('cart-page') }}" class="main selected menu-name">{!! trans('frontend.cart') !!}</a></li> -->
                  <li class="mini-cart-content">
                      @include('pages.ajax-pages.mini-cart-login')
                  </li>
                @else
                  <!-- <li><a href="{{ route('cart-page') }}" class="main menu-name">{!! trans('frontend.cart') !!}</a></li> -->
                   <li class="mini-cart-content">
                      @include('pages.ajax-pages.mini-cart-login')
                  </li>
                @endif
              @else
                  @if(Request::is('cart'))
                  <!-- <li><a href="{{ route('cart-page') }}" class="main selected menu-name">{!! trans('frontend.cart') !!}</a></li> -->
                  <li class="mini-cart-content">
                      @include('pages.ajax-pages.mini-cart-html')
                  </li>
                @else
                  <!-- <li><a href="{{ route('cart-page') }}" class="main menu-name">{!! trans('frontend.cart') !!}</a></li> -->
                   <li class="mini-cart-content">
                      @include('pages.ajax-pages.mini-cart-html')
                  </li>
                @endif
              @endif

             <!--  @if(Request::is('blogs'))
                <li><a href="{{ route('blogs-page-content') }}" class="main selected menu-name">{!! trans('frontend.blog') !!}</a></li>
              @else
                <li><a href="{{ route('blogs-page-content') }}" class="main menu-name">{!! trans('frontend.blog') !!}</a></li>
              @endif -->

              @if(count($pages_list) > 0)
              <!-- <li>
                <div class="dropdown custom-page">
                  <a class="dropdown-toggle menu-name" href="#" data-hover="dropdown" data-toggle="dropdown"> {!! trans('frontend.pages_label') !!} 
                    <span class="caret"></span>
                  </a>
                  <ul class="dropdown-menu">
                    @foreach($pages_list as $pages)
                    <li> <a href="{{ route('custom-page-content', $pages['post_slug']) }}">{!! $pages['post_title'] !!}</a></li>
                    @endforeach
                  </ul>
                </div>
              </li> -->
              @endif
              
              

                <li>
                  @include('pages.ajax-pages.search-page')
                </li>

              <li class="users-vendors-login"><a href="#" class="main dropdown-toggle" data-toggle="dropdown" data-hover="dropdown"><i class="fa fa-user" aria-hidden="true"></i> <span class="hidden-xs"></span><span class="caret"></span></a>
                  <ul class="dropdown-menu top_menu text-center">
                    @if (Session::has('shopist_frontend_user_id'))
                    <li><a href="{{ route('user-dashboard-page') }}">{{Session::get('shopist_frontend_user_name')}}</li>
                    <li><a href="{{ route('my-orders-page') }}"><i class="fa fa-file-text-o"></i>&nbsp;{!! trans('frontend.user_order_label') !!}</a></li>
                    <li><a href="{{ route('my-saved-items-page') }}"><i class="fa fa-heart"></i>&nbsp;{!! trans('frontend.user_wishlist') !!}</a></li>
                    <form method="post" action="/user/account/logout" enctype="multipart/form-data">
                      <input type="hidden" name="_token" id="_token" value="{{csrf_token()}}">  
                    <li>
                      <button type="submit" class="btn btn-default btn-block"><i class="fa fa-power-off"></i> Sign out</button>
                    </li>
                    </form>
                    @else
                      @if (Session::has('shopist_admin_user_id') && !empty(get_current_vendor_user_info()['user_role_slug']) && get_current_vendor_user_info()['user_role_slug'] == 'vendor')
                       <li><a target="_blank" href="{{ route('admin.dashboard') }}">{!! trans('frontend.vendor_account_label') !!}</a></li>
                      @else
                       <li>
                        <a class="login-head-btn" href="{{ route('user-login-page') }}">{!! trans('frontend.frontend_user_signin') !!}</a>
                       </li>
                        <li class="text-center">
                          <small>New Customer? <a href="{{ route('user-registration-page')}}">Start here</a></small>
                          </li>
                          <li><hr></li>
                       <li><a target="_blank" href="{{ route('vendor.login') }}">{!! trans('frontend.frontend_vendor_login') !!}</a></li>
                        <!-- <li><a href="{{ route('vendor-registration-page') }}">{!! trans('frontend.vendor_registration') !!}</a></li> -->
                       
                      @endif
                    @endif
                    
                    
                    
                   
                  </ul>
                </li>



            </ul>
          </div>
        </nav>
      </div>
    
         
      </div>
    </div>    
  </div>    
   
    
</div>

@if($appearance_settings_data['header_details']['slider_visibility'] == true && Request::is('/'))
  <?php $count = count(get_appearance_header_settings_data());?>
  @if($count > 0)
  <div class="header-with-slider">
    <!-- social div -->
    <div class="container-fluid banner_social_container hidden-xs hidden-sm">
    <div class="col-md-12">
        <ul id="social_icons">
            <li><a href="//{{ $appearance_all_data['footer_details']['follow_us_url']['fb'] }}" class="facebook"><i class="fa fa-facebook"></i></a></li>
            <li><a href="//{{ $appearance_all_data['footer_details']['follow_us_url']['twitter'] }}" class="twitter"><i class="fa fa-twitter"></i></a></li>
            <li><a href="//{{ $appearance_all_data['footer_details']['follow_us_url']['linkedin'] }}" class="linkedin"><i class="fa fa-linkedin"></i></a></li>
            <li><a href="//{{ $appearance_all_data['footer_details']['follow_us_url']['youtube'] }}" class="youtube"><i class="fa fa-youtube-play"></i></a></li>
        </ul>
    </div>
</div>
    <div id="slider-carousel" class="carousel slide" data-ride="carousel">
      <ol class="carousel-indicators">
        @for($i = 0; $i < $count; $i++)
          @if($i== 0)
            <li data-target="#slider-carousel" data-slide-to="{{ $i }}" class="active"></li>
          @else
            <li data-target="#slider-carousel" data-slide-to="{{ $i }}"></li>
          @endif
        @endfor                           
      </ol>

      <?php $numb = 1; ?>
      <div class="carousel-inner">
        @foreach(get_appearance_header_settings_data() as $img)
        
          @if($numb == 1)
            <div class="item active">

              @if($img->img_url)
                <?php if(isset($img->text)){?>
                  <a href="{!! $img->text !!}">
                <?php }?>
                <img src="{{ $img->img_url }}" class="girl img-responsive" alt="" />
                <?php if(isset($img->text)){?>
                  </a>
                <?php }?>
              @endif

              <?php if(isset($img->text)){?>
                <!-- <div class="dynamic-text-on-image-container">{!! $img->text !!}</div> -->
              <?php }?>
            </div>
          @else
            <div class="item">
              @if($img->img_url)
                <img src="{{ get_image_url($img->img_url) }}" class="girl img-responsive" alt="" />
              @endif

              <?php if(isset($img->text)){?>
                <!-- <div class="dynamic-text-on-image-container">{!! $img->text !!}</div> -->
              <?php }?>
            </div>
          @endif
          <?php $numb++ ; ?>
          
        @endforeach
      </div>
    </div>
  </div>
  @endif
@endif