@elseif($numb == 2)
                      <div class="product-content">
                        <div class="image-content text-center">
                          @if(get_product_image($row['id']))
                          <img src="{{ get_image_url(get_product_image($row['id'])) }}" alt="{{ basename(get_product_image($row['id'])) }}" />
                          @else
                          <img src="{{ default_placeholder_img_src() }}" alt="" />
                          @endif
                        </div>
                        <div class="product-details">
                          <p>{{ get_product_title($row['id']) }}</p>
                          @if(get_product_type($row['id']) == 'simple_product')
                            <p><strong>{!! price_html( get_product_price($row['id']), get_frontend_selected_currency() ) !!}</strong></p>
                          @elseif(get_product_type($row['id']) == 'configurable_product')
                            <p><strong>{!! get_product_variations_min_to_max_price_html(get_frontend_selected_currency(), $row['id']) !!}</strong></p>
                          @elseif(get_product_type($row['id']) == 'customizable_product' || get_product_type($row['id']) == 'downloadable_product')
                            @if(count(get_product_variations($row['id']))>0)
                              <p><strong>{!! get_product_variations_min_to_max_price_html(get_frontend_selected_currency(), $row['id']) !!}</strong></p>
                            @else
                              <p><strong>{!! price_html( get_product_price($row['id']), get_frontend_selected_currency() ) !!}</strong></p>
                            @endif
                          @endif
                          <a href="{{ route('details-page', $row['post_slug'])}}" class="btn btn-sm btn-style btn-vibha-inverse">{!! trans('frontend.read_more_label') !!}</a>
                        </div>
                      </div>


<!-- for new arrival -->
@elseif($numb == 2)
                      <div class="product-content">
                        <div class="image-content text-center">
                          @if(get_product_image($row['id']))
                          <img src="{{ get_image_url(get_product_image($row['id'])) }}" alt="{{ basename(get_product_image($row['id'])) }}" />
                          @else
                          <img src="{{ default_placeholder_img_src() }}" alt="" />
                          @endif
                        </div>
                        <div class="product-details">
                          <p>{{ get_product_title($row['id']) }}</p>
                          @if(get_product_type($row['id']) == 'simple_product')
                            <p><strong>{!! price_html( get_product_price($row['id']), get_frontend_selected_currency() ) !!}</strong></p>
                          @elseif(get_product_type($row['id']) == 'configurable_product')
                            <p><strong>{!! get_product_variations_min_to_max_price_html(get_frontend_selected_currency(), $row['id']) !!}</strong></p>
                          @elseif(get_product_type($row['id']) == 'customizable_product' || get_product_type($row['id']) == 'downloadable_product')
                            @if(count(get_product_variations($row['id']))>0)
                              <p><strong>{!! get_product_variations_min_to_max_price_html(get_frontend_selected_currency(), $row['id']) !!}</strong></p>
                            @else
                              <p><strong>{!! price_html( get_product_price($row['id']), get_frontend_selected_currency() ) !!}</strong></p>
                            @endif
                          @endif
                          
                          <a href="{{ route('details-page', $row['post_slug'])}}" class="btn btn-sm btn-style btn-vibha-inverse">{!! trans('frontend.read_more_label') !!}</a>
                        </div>
                      </div>