<a href="javascript:void(0)" class="main show_search_popup"> <span class="hidden-xs"><i class="fa fa-search" aria-hidden="true"></i></span> <span class="hidden-sm hidden-md hidden-lg fa fa-search fa-lg"></span></a>
<div id="search_popup" class="bottom">
  <i class="fa fa-times close_search" aria-hidden="true"></i>
  <div class="col-xs-8 col-sm-10 col-md-8 col-lg-8 pull-right">
    <h3 class="search_heading">Looking for something specific? Your search starts here...</h3>
          <form id="search_option" action="{{ route('shop-page') }}" method="get">
            <div class="input-group">
              <input type="text" id="srch_term" name="srch_term" class="form-control" placeholder="{{ trans('frontend.search_for_label') }}">
              <span class="input-group-btn">
                <button id="btn-search" type="submit" class="btn btn-default">
                  SEARCH
                </button>
              </span>
            </div>
          </form>
    <h3 class="sml">OR</h3>
    <h3 class="search_heading">Ordered as guest? Track your order</h3>
	<h3><a class="wht" href="{{ route('my-orders-page') }}">HERE !!! </a></h3>
  </div> 
</div>