

<a href="" class="main show-mini-cart"> <span class="hidden-xs"><i class="fa fa-shopping-cart" aria-hidden="true"></i></span> <span class="hidden-sm hidden-md hidden-lg fa fa-shopping-cart fa-lg"></span> <span class="cart-count"><span id="total_count_by_ajax">{!! get_user_cart_count() !!}</span></span></a>
<div id="list_popover" class="bottom">
  <div class="arrow"></div>
  @if(isset($data)) <!-- if data is set from ajax call -->
    @if( count($data) >0 )
      <div id="cd-cart">
        <h2>{!! trans('frontend.mini_cart_label_cart') !!}</h2>
        <ul class="cd-cart-items">
          @foreach($data as $d)
            <li>
              <span class="cd-qty">{!! $d->quantity !!}x</span>&nbsp;  {!! str_limit(($d->name), $limit = 15, $end = '...') !!}
              <div class="cd-price">{!! price_html( get_product_price_html_by_filter( Cart::getRowPrice($d->quantity, get_role_based_price_by_product_id($d ->product_id, $d->price))) ) !!}</div>
              <a href="{{ route('removed-item-from-cart', $d->product_id)}}" class="cd-item-remove cd-img-replace cart_quantity_delete"></a>
            </li>
          @endforeach
        </ul>

        <div class="cd-cart-total">
          <p class="no-upper">{!! trans('frontend.total') !!} <span>{!! price_html( get_total_cart_price($data) ) !!}</span></p>
        </div>

        <a href="{{ route('checkout-page') }}" class="checkout-btn">{!! trans('frontend.checkout') !!}</a>
      </div>
    @else
      <div class="empty-cart-msg">{{ trans('frontend.empty_cart_msg') }}</div>
    @endif
  @else
    @if(count(get_user_cart_products()) > 0)
    <div id="cd-cart">
        <h2>{!! trans('frontend.mini_cart_label_cart') !!}</h2>
        <ul class="cd-cart-items">
      @foreach(get_user_cart_products() as $d)
            <li>
              <span class="cd-qty">{!! $d->quantity !!}x</span>&nbsp;{!! $d->name !!}
              @if($d->order_price == 0)
                <div class="cd-price">{!! price_html( get_product_price_html_by_filter( Cart::getRowPrice($d->quantity, get_role_based_price_by_product_id($d ->product_id, $d->price))) ) !!}</div>
              @else
                <div class="cd-price">{!! price_html( get_product_price_html_by_filter( Cart::getRowPrice($d->quantity, get_role_based_price_by_product_id($d ->product_id, $d->order_price))) ) !!}</div>
              @endif
              
              <a href="{{ route('removed-item-from-cart', $d->product_id)}}" class="cd-item-remove cd-img-replace cart_quantity_delete"></a>
            </li>
      @endforeach
        </ul>
        <div class="cd-cart-total">
          <p class="no-upper">{!! trans('frontend.total') !!} <span>{!! price_html( get_total_cart_price(get_user_cart_products()) ) !!}</span></p>
        </div>

        <a href="{{ route('checkout-page') }}" class="checkout-btn">{!! trans('frontend.checkout') !!}</a>
      </div>
    @else
      <div class="empty-cart-msg">{{ trans('frontend.empty_cart_msg') }}</div>
    @endif
  @endif
</div>