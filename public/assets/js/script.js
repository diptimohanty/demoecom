function openNav() {
  document.getElementById("myNav").style.width = "100%";
}
function closeNav() {
  document.getElementById("myNav").style.width = "0%";
}

$(document).ready(function(){
	$('.kurvzCategory').hover(function(){
		$(this).siblings().addClass('kurvzCategoryhover');
		$(this).removeClass('kurvzCategoryhover');
	},function(){
		$(this).addClass('kurvzCategoryhover');
	});
	$('#KurvzMainCategory').mouseleave(function(){
		$(this).find('.kurvzCategoryhover').removeClass('kurvzCategoryhover');
	});
	/*
	* For sticky header
	*/
	$(window).scroll(function(){
	  var sticky = $('.KurvzLogo'),
	      scroll = $(window).scrollTop();

	  if (scroll >= 100){
	  	sticky.addClass('fixed');
	  	$('.KurvzLogo a img').attr('src','images/leaf_logo.png')
	  }else{
	  	sticky.removeClass('fixed');
	  	$('.KurvzLogo a img').attr('src','images/Orbio Website/Logo/WhiteLogo-1.png')
	  } 
	});


});
$(window).on("load", function() {
	// IMAGE FADING
	// $('.mustHaveOne .backImage').show().fadeOut(1);
	$('.mustHaveOne').hover(function () {
	    $(this).find('.backImage').fadeIn(500);
	}, function(){
	    $(this).find('.backImage').fadeOut(500);
	});
});
