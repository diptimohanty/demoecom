$('.homeFeaturedProductSelectorLink').click(function(){
    $('.homeFeaturedProductSelectorLink').removeClass('active');
    $(this).addClass('active');
    $('.homeFeaturedProductsIn').fadeOut().fadeIn();
});
$('.toggleNavigationLink').click(function () {
    $('body').css({'overflow':'hidden'});
    $('.kurvzMenuDiv').animate({'left': '0'});
});
$('.kurvzMenuClose').click(function () {
    $('body').css({'overflow':'auto'});
    $('.kurvzMenuDiv').animate({'left': '-100%'});
    $('.kurvzMenuDiv');
});
var homeTestimonialCarouselCount = 2;
function homeTestimonialCarousel(action) {
    console.log(homeTestimonialCarouselCount);
    if(action == 0) {
        if (homeTestimonialCarouselCount > 1) {
            homeTestimonialCarouselCount--;
            $('.homeTestimonialsImgCarousel .homeTestimonialImgDiv, .homeTestimonialsContentCarousel .homeTestimonialContentDiv').removeClass('active');
            $('.homeTestimonialsImgCarousel .homeTestimonialImgDiv:nth-child(' + homeTestimonialCarouselCount + '), .homeTestimonialsContentCarousel .homeTestimonialContentDiv:nth-child(' + homeTestimonialCarouselCount + ')').addClass('active');
        } else {
            return false;
        }
    } else if (action == 1) {
        if (homeTestimonialCarouselCount < 3) {
            homeTestimonialCarouselCount++;
            $('.homeTestimonialsImgCarousel .homeTestimonialImgDiv, .homeTestimonialsContentCarousel .homeTestimonialContentDiv').removeClass('active');
            $('.homeTestimonialsImgCarousel .homeTestimonialImgDiv:nth-child(' + homeTestimonialCarouselCount + '), .homeTestimonialsContentCarousel .homeTestimonialContentDiv:nth-child(' + homeTestimonialCarouselCount + ')').addClass('active');
        } else {
            return false;
        }
    }
}