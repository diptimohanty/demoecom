<?php

namespace shopist\Http\Controllers;

use Illuminate\Http\Request;
use DB;
use Session;
use Anam\Phpcart\Facades\Cart;


class CartData extends Controller
{
    public function save_to_cart($cart_data){
    	//$data = new CartModel();
    	//dd($cart_data[0]['img_src']);

        $check_product = DB::table('cart_data')->where(['product_id'=>$cart_data[0]['product_id'],'user_id'=>Session::get('shopist_frontend_user_id')])->get()->first();

        if(count($check_product) > 0){
            $qty = (int)$check_product->quantity;
            $qty = (int)($qty + $cart_data[0]['quantity']);
            //print_r($check_product->quantity);
            $resp = DB::table('cart_data')->where(['product_id'=>$cart_data[0]['product_id'],'user_id'=>Session::get('shopist_frontend_user_id')])->update(['quantity'=>$qty]);
            
        }else{
            $resp = DB::table('cart_data')->insert(
                [
                    'name'          =>$cart_data[0]['name'],
                    'product_id'    =>$cart_data[0]['product_id'],
                    'user_id'       =>Session::get('shopist_frontend_user_id'),
                    'img_src'       =>$cart_data[0]['img_src'],
                    'quantity'      =>$cart_data[0]['quantity'],
                    'price'         =>$cart_data[0]['price'],
                    'order_price'   =>$cart_data[0]['order_price'],
                    'variation_id'  =>$cart_data[0]['variation_id'],
                    
                    'tax'           =>$cart_data[0]['tax'],
                    'product_type'  =>$cart_data[0]['product_type'],
                    'acces_token'   =>$cart_data[0]['acces_token'],
                    'product_color' =>$cart_data[0]['product_color'],
                    'product_tax'   =>$cart_data[0]['product_tax'],
                ]
            );
        }
    	
    	//$data->insert([]);

        if($resp){
            echo "item_added";
        }else{

        }
    }

    public static function getTax(){
        $data = DB::table('cart_data')->where('user_id','=',Session::get('shopist_frontend_user_id'))->get();
        $tax_amt = 0;
        foreach($data as $d){
            if($d->order_price !=0){
                $tax_amt += ($d->order_price*$d->quantity) * ($d->product_tax/100); 
            }else{
                $tax_amt += ($d->price*$d->quantity) * ($d->product_tax/100); 

            }
        }
        return $tax_amt;
    }

    public function updateCartProduct($product_id,$regular_price,$sale_price,$tax_per){
        if($sale_price == 0){
            $sale_price = $regular_price;
        }
        DB::table('cart_data')->where('product_id','=',$product_id)->update([
            'price'       => $regular_price,
            'order_price' => $sale_price,
            'product_tax' => $tax_per  

        ]);
    }


    public static function getGrandTotal(){
        

        $data = DB::table('cart_data')->where('user_id','=',Session::get('shopist_frontend_user_id'))->get();
        $tax_amt = 0;
        $tot_amt = (float)0;
        foreach($data as $d){
            if($d->order_price !=0){
                $tax_amt += ($d->order_price*$d->quantity) * ($d->product_tax/100); 
                $pr = (float)($d->order_price);
            }else{
                $tax_amt += ($d->price*$d->quantity) * ($d->product_tax/100); 
                $pr = (float)($d->price);
            }
            $tot_amt += ($pr*$d->quantity); 
        }

        return $tot_amt + $tax_amt;

    }

    public static function getAllProducts(){
        return DB::table('cart_data')->where('user_id','=',Session::get('shopist_frontend_user_id'))->get();
    }

    public static function checkUser(){
        return Session::has('shopist_frontend_user_id');
    }

    public static function applyCoupon($amount,$total){
        return $total-$amount;
    }

    public static function getSessionCartItems(){
        return Cart::getItems();
    }

    public static function saveSessionCartToDb($data){
        //dd($data);
        if($data != ""){
            foreach ($data as $items) {
                print_r($items->product_id);
                $check_product = DB::table('cart_data')->where(['product_id'=>$items->product_id,'user_id'=>Session::get('shopist_frontend_user_id')])->get()->first();

                if(count($check_product) > 0){
                    $qty = (int)$check_product->quantity;
                    $qty = (int)($qty + $items->quantity);
                    //print_r($check_product->quantity);
                    $resp = DB::table('cart_data')->where(['product_id'=>$items->product_id,'user_id'=>Session::get('shopist_frontend_user_id')])->update(['quantity'=>$qty]);
                    
                }else{
                    $resp = DB::table('cart_data')->insert(
                        [
                            'name'          =>$items->name,
                            'product_id'    =>$items->product_id,
                            'user_id'       =>Session::get('shopist_frontend_user_id'),
                            'img_src'       =>$items->img_src,
                            'quantity'      =>$items->quantity,
                            'price'         =>$items->price,
                            'order_price'   =>$items->order_price,
                            'variation_id'  =>$items->variation_id,
                            
                            'tax'           =>$items->tax,
                            'product_type'  =>$items->product_type,
                            'acces_token'   =>$items->acces_token,
                            'product_color' =>$items->product_color,
                            'product_tax'   =>$items->product_tax,
                        ]
                    );
                }
        }
        //dd($data);
        //clear cart session
        }
        
    }

    public static function clearCart(){
        return DB::table('cart_data')->where('user_id','=',Session::get('shopist_frontend_user_id'))->delete();
    }
}
