<?php

namespace shopist\Http\Controllers;

use Illuminate\Http\Request;
use shopist\Models\PostExtra;
use DB;

class EditOrderController extends Controller
{
    //function to edit billing address

    public function editBilligAddress(Request $request){
    	$order_id = $request->input('order_id');
    	$_address_1 = $request->input('address_1');
    	$_address_2 = $request->input('address_2');
    	$_billing_phone = $request->input('_billing_phone');
    	$_billing_postcode = $request->input('_billing_postcode');
    	$_billing_city = $request->input('_billing_city');
    	$flag = 0;
    	/*
			address line 1
    	*/
    	if( PostExtra::where('post_id',$order_id)->where('key_name','_billing_address_1')->update(['key_value' => $_address_1]) ){
    	}else{
    		$flag=1;
    	}

    	/*
			address line 2
    	*/
    	if (PostExtra::where('post_id',$order_id)->where('key_name','_billing_address_2')->update(['key_value' => $_address_2])){

    	}else{
    		$flag=1;
    	}


    	/*
			billing city
    	*/
    	if( PostExtra::where('post_id',$order_id)->where('key_name','_billing_city')->update(['key_value' => $_billing_city])){

    	}else{
    		$flag=1;
    	}


    	/*
			country
    	*/
    	if( PostExtra::where('post_id',$order_id)->where('key_name','_billing_phone')->update(['key_value' => $_billing_phone]) ){

    	}else{
    		$flag=1;
    	}


    	/*
			zip
    	*/
    	if( PostExtra::where('post_id',$order_id)->where('key_name','_billing_postcode')->update(['key_value' => $_billing_postcode]) ){

    	}else{
    		$flag=1;
    	}

    	$resp[] = array('status'=>200,'flag'=>$flag);
    	return json_encode($resp);

    }
}
