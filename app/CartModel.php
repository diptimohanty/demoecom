<?php

namespace shopist;

use Illuminate\Database\Eloquent\Model;

class CartModel extends Model
{
    protected $table="cart_data";
}
